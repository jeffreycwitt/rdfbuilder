#!/usr/bin/env ruby

#required dependencies
require 'rubygems'
require 'nokogiri'
require_relative 'rdfconfig.rb'

#config info -- this should eventually go in a separate config file
t = ConfigInfo.new
$base_location = t.base_location

#$base_location = "/Users/JCWitt/Documents/PlaoulTranscriptions/"
$trans_base_location = "/Users/JCWitt/Documents/PlaoulTranslations/"

def extract_info(filestem, xpath, prefix)
	
	if prefix != 'none'
	full_filename = "#{$base_location}#{filestem}/#{prefix}_#{filestem}.xml"
	else
	full_filename = "#{$base_location}#{filestem}/#{filestem}.xml"
	end

	f = File.open("#{full_filename}", "r")  
    doc = Nokogiri::XML(f)
    f.close
    node_value = ""
    doc.xpath("#{xpath}").each do |node|
	node_value = node.content	
	end
    return node_value

end

def make_rdf_xml(date, filestem)
	full_filename = "#{filestem}.xml"
	document_title = extract_info("#{filestem}", "//xmlns:titleStmt/xmlns:title", 'none')
	amp = "&"
	bitfs = "#{filestem}".downcase
	builder = Nokogiri::XML::Builder.new do |xml|
	
	xml.comment("This RDF file was created on #{date} by RDFbuilder: a ruby script written by Jeffrey C. Witt 
		RDFbuilder is publically available at http://bitbucket.org/jeffreycwitt/rdfbuilder  
		Please contact Jeffrey C. Witt at jcwitt@loyola.edu if you would like to help develop this script for a wider audience")

	  	xml.RDF("xmlns:rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#", 
	  	"xmlns:role" => "http://www.loc.gov/loc.terms/relators/",
	  	"xmlns:rdfs" => "http://www.w3.org/2000/01/rdf-schema#",
	    "xmlns:plaoul" =>"http://petrusplaoul.org",
	    "xmlns:collex" =>"http://www.collex.org/schema#",
	    "xmlns:dcterms" =>"http://purl.org/dc/terms/",
	    "xmlns:dc" => "http://purl.org/dc/elements/1.1/") { 
    
    #this line is need to add the namespace prefix to the root element
	    	xml.parent.namespace = xml.parent.namespace_definitions.find{|ns|ns.prefix=="rdf"}
	    
		    xml['plaoul'].project("rdf:about" => "http://petrusplaoul.org/text/uri/edited/#{filestem}") {
			    xml['dc'].title "#{document_title}"
			    xml['dc'].language "Latin"
				xml['dc'].date "1392"
				xml['dc'].type "Codex"
				xml['dc'].subject "Philosophy"
				xml['dc'].subject "Theology"
				xml['role'].AUT "Petrus Plaoul"
				xml['role'].EDT "Witt, Jeffrey"
				xml['collex'].genre "Philosophy"
				xml['collex'].genre "Religion"
				xml['collex'].discipline "Philosophy"
				xml['collex'].discipline "Religious Studies"
				xml['collex'].federation "MESA"
				xml['collex'].archive "Plaoul"
				xml['collex'].source_xml("rdf:resource" => "https://bitbucket.org/jeffreycwitt/#{bitfs}/raw/master/#{filestem}.xml")
				xml['collex'].text_("rdf:resource" => "http://petrusplaoul.org/plaintext/index.php?fs=#{filestem}#{amp}ms=edited")
				xml['dcterms'].isPartOf("rdf:resource" => "http://petrusplaoul.org") 
				if File.exists?("#{$base_location}#{filestem}/reims_#{filestem}.xml")
					xml['dcterms'].hasPart("rdf:resource" => "http://petrusplaoul.org/text/uri/reims/#{filestem}") 
				end
				if File.exists?("#{$base_location}#{filestem}/vat_#{filestem}.xml")
					xml['dcterms'].hasPart("rdf:resource" => "http://petrusplaoul.org/text/uri/vat/#{filestem}") 
				end
				if File.exists?("#{$base_location}#{filestem}/svict_#{filestem}.xml")	
					xml['dcterms'].hasPart("rdf:resource" => "http://petrusplaoul.org/text/uri/svict/#{filestem}")
				end	
				if File.exists?("#{$base_location}#{filestem}/sorb_#{filestem}.xml")
					xml['dcterms'].hasPart("rdf:resource" => "http://petrusplaoul.org/text/uri/sorb/#{filestem}") 
				end
				#if File.exists?("#{$trans_base_location}trans_engl_#{filestem}/trans_engl_#{filestem}.xml")
				#	xml['dcterms'].hasPart("rdf:resource" => "http://petrusplaoul.org/text/uri/engltranslation/#{filestem}") {xml.text "#{document_title} [Reims Transcription]"}
				#end	
					xml['rdfs'].seeAlso("rdf:resource" => "http://petrusplaoul.org/text/textdisplay.php?fs=#{filestem}")
	    }
	} 
end

end


def write_rdf_all(date)
		print "test"
		file = "/Users/JCWitt/WebPages/petrusplaoul-mirror/projectfiles/projectdata.xml"
		f = File.open("#{file}", "r")  
    	doc = Nokogiri::XML(f)
		fsList = doc.xpath("//div[@id='body']//fileName/@filestem").each do |filestem|
    	
    	
    		print "#{filestem}"
    		write_rdf("#{date}", "#{filestem}", "none")
    		if File.exists?("#{$base_location}/#{filestem}/reims_#{filestem}.xml")
    			write_rdf("#{date}", "#{filestem}", "reims")
    		end
			if File.exists?("#{$base_location}/#{filestem}/vat_#{filestem}.xml")
				write_rdf("#{date}", "#{filestem}", "vat")
			end
			if File.exists?("#{$base_location}/#{filestem}/sorb_#{filestem}.xml")		
				write_rdf("#{date}", "#{filestem}", "sorb")
			end
			if File.exists?("#{$base_location}/#{filestem}/svict_#{filestem}.xml")
				write_rdf("#{date}", "#{filestem}", "svict")
			end
		end
		f.close

	end


def write_rdf(date, filestem, prefix)
	

	if prefix != 'none'
	builder = make_rdf_dipl_xml("#{date}", "#{filestem}", "#{prefix}")
	o = File.new("/Users/JCWitt/Documents/PlaoulRDFFiles/rdf_#{prefix}_#{filestem}.xml", "w")
	else
	builder = make_rdf_xml("#{date}", "#{filestem}")
	o = File.new("/Users/JCWitt/Documents/PlaoulRDFFiles/rdf_#{filestem}.xml", "w") 
	end
	o.write(builder.to_xml :indent => 3, :encoding => 'UTF-8')
	o.close
	puts "it seems to have worked"


end

def make_rdf_dipl_xml(date, filestem, prefix)
	amp = "&"
	full_filename = "#{prefix}_#{filestem}.xml"
	document_title = extract_info("#{filestem}", "//xmlns:titleStmt/xmlns:title", "#{prefix}")
	main_doc_title = extract_info("#{filestem}", "//xmlns:titleStmt/xmlns:title", "none")
	bitfs = "#{filestem}".downcase
	if prefix == 'sorb'
		manuscript = "sorbonne"
		msabbrev = "sorb"
	elsif prefix == 'reims'
		manuscript = "reims"
		msabbrev = "reims"
	elsif prefix == 'vat'
		manuscript = 'vatican'
		msabbrev = "vat"
	elsif prefix == 'svict'
		manuscript = 'stvictor'
		msabbrev = "svict"
	end

	
	builder = Nokogiri::XML::Builder.new do |xml|
	
	xml.comment("This RDF file was created on #{date} by RDFbuilder: a ruby script written by Jeffrey C. Witt 
		RDFbuilder is publically available at http://bitbucket.org/jeffreycwitt/rdfbuilder  
		Please contact Jeffrey C. Witt at jcwitt@loyola.edu if you would like to help develop this script for a wider audience")

  	xml.RDF("xmlns:rdf" => "http://www.w3.org/1999/02/22-rdf-syntax-ns#", 
  	"xmlns:role" => "http://www.loc.gov/loc.terms/relators/",
  	"xmlns:rdfs" => "http://www.w3.org/2000/01/rdf-schema#",
    "xmlns:plaoul" =>"http://petrusplaoul.org",
    "xmlns:collex" =>"http://www.collex.org/schema#",
    "xmlns:dcterms" =>"http://purl.org/dc/terms/",
    "xmlns:dc" => "http://purl.org/dc/elements/1.1/") { 
    
    #this line is need to add the namespace prefix to the root element
    xml.parent.namespace = xml.parent.namespace_definitions.find{|ns|ns.prefix=="rdf"}
    
    xml['plaoul'].project("rdf:about" => "http://petrusplaoul.org/text/uri/#{prefix}/#{filestem}"){
    xml['dc'].title "#{document_title}"
	xml['dc'].language "Latin"
	xml['dc'].date "1392"
	xml['dc'].type "Manuscript"
	xml['dc'].subject "Philosophy"
	xml['dc'].subject "Theology"
	xml['role'].AUT "Petrus Plaoul"
	xml['role'].EDT "Witt, Jeffrey"
	xml['collex'].genre "Philosophy"
	xml['collex'].genre "Religion"
	xml['collex'].discipline "Philosophy"
	xml['collex'].discipline "Religious Studies"
	xml['collex'].federation "MESA"
	xml['collex'].archive "Plaoul"
	xml['collex'].source_xml("rdf:resource" => "https://bitbucket.org/jeffreycwitt/#{bitfs}/raw/master/#{prefix}_#{filestem}.xml")
	xml['collex'].text_("rdf:resource" => "http://petrusplaoul.org/plaintext/index.php?fs=#{filestem}#{amp}ms=#{msabbrev}")
	xml['dcterms'].isPartOf("rdf:resource" => "http://petrusplaoul.org/text/uri/edited/#{filestem}") 
	xml['rdfs'].seeAlso("rdf:resource" => "http://petrusplaoul.org/text/textdisplay.php?fs=#{filestem}&ms=#{msabbrev}")
    }
}
end	

end

	